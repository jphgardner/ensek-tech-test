﻿using Ensek.Api.Core.Database;
using Ensek.Api.Core.Models;
using MongoDB.Driver;

namespace Ensek.Api.Core.Collections
{
    public class AccountCollection: ICollection<Account>
    {
        public AccountCollection(Database.Database database) : base(database)
        {
        }

        public bool AccountIdExists(int accountId)
        {
            var accounts = Collection.Find(account => account.AccountId == accountId).ToList();
            return accounts.Count == 1;
        }
    }
}