﻿using Ensek.Api.Core.Database;
using Ensek.Api.Core.Models;
using MongoDB.Driver;

namespace Ensek.Api.Core.Collections
{
    public class ReadingCollection : ICollection<Reading>
    {
        public ReadingCollection(Database.Database database) : base(database)
        {
        }

        public bool Exists(Reading reading)
        {
            return Collection.Find(r =>
                    r.AccountId == reading.AccountId && 
                    r.Value == reading.Value &&
                    r.DateTime == reading.DateTime)
                .CountDocuments() == 1;
        }

        public IMongoCollection<Reading> GetCollection() => Collection;
    }
}