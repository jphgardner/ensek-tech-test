﻿using System;
using Ensek.Api.Core.Database;
using Ensek.Api.Core.Interfaces;
using Ensek.Api.Core.Serializers;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using MongoDB.Bson.Serialization;
using MongoDB.Driver;

namespace Ensek.Api.Core.Extensions
{
    public static class DatabaseExtensions
    {

        public static void AddMongoDb(this IServiceCollection services, IConfiguration configuration)
        {
            //Register Custom BsonSerializer For DateTime objects
            BsonSerializer.RegisterSerializer(typeof(DateTime), new EnsekDateTimeSerializer());
            
            services.Configure<DatabaseOptions>(configuration.GetSection("Database"));
            services.AddSingleton<IMongoClient, MongoClient>(c => 
            {
                var options = c.GetService<IOptions<DatabaseOptions>>();
                return new MongoClient(options.Value.ConnectionString);
            });
            services.AddSingleton<Database.Database>();
        }

        public static void AddCollection<TModel, TCollection>(this IServiceCollection services)
            where TModel: IModel
            where TCollection: ICollection<TModel>
        {
            services.AddSingleton<TCollection>();
        }

    }
}