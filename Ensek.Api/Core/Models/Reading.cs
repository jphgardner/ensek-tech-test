﻿using System;
using Ensek.Api.Core.Extensions;
using Ensek.Api.Core.Interfaces;

namespace Ensek.Api.Core.Models
{
    public class Reading: IModel
    {
        public int AccountId { get; set; }
        public DateTime DateTime { get; set; }

        public int Value { get; set; }

        public Reading(int accountId, DateTime dateTime, int value)
        {
            AccountId = accountId;
            DateTime = dateTime;
            Value = value;
        }

        public Reading()
        {
        }
    }
}