﻿using Ensek.Api.Core.Models;
using TinyCsvParser.Mapping;

namespace Ensek.Api.Core.CSVMappers
{
    public class ReadingMapping : CsvMapping<Reading>
    {
        public ReadingMapping() : base()
        {
            MapProperty(0, reading => reading.AccountId);
            MapProperty(1, reading => reading.DateTime);
            MapProperty(2, reading => reading.Value);
        }
    }
}