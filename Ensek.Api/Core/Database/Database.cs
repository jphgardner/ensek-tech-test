﻿using Microsoft.Extensions.Options;
using MongoDB.Driver;

namespace Ensek.Api.Core.Database
{
    public class Database
    {
        private IMongoClient _client;
        private IMongoDatabase _database;

        public Database(IMongoClient client, IOptions<DatabaseOptions> options)
        {
            _client = client;
            _database = _client.GetDatabase(options.Value.DatabaseName);
        }

        public IMongoCollection<TModel> GetCollection<TModel>(string collection) =>
            _database.GetCollection<TModel>(collection);
    }
}