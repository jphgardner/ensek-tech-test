﻿using System.Collections.Generic;
using Ensek.Api.Core.Interfaces;
using MongoDB.Driver;

namespace Ensek.Api.Core.Database
{
    public abstract class ICollection<TModel>
        where TModel: IModel
    {
        protected IMongoCollection<TModel> Collection;

        protected ICollection(Database database)
        {
            Collection = database.GetCollection<TModel>(typeof(TModel).Name);
        }
        
        public List<TModel> Find(FilterDefinition<TModel> definition, ProjectionDefinition<TModel> projection = null)
        {
            if (projection != null)
            {
                return Collection.Find<TModel>(definition).Project<TModel>(projection).ToList();
            }
            else
            {
                return Collection.Find<TModel>(definition).ToList();
            }
        }
        
        public List<TModel> Get() => Collection.Find(FilterDefinition<TModel>.Empty).ToList();
        public TModel GetById(string id) => Collection.Find(doc => doc.Id == id).FirstOrDefault();
        
        public bool Exists(string id) => Collection.Find(doc => doc.Id == id).CountDocuments() == 1;
        
        public TModel Create(TModel data)
        {
            Collection.InsertOne(data);
            return data;
        }

        public TModel Update(string id, TModel data)
        {
            Collection.ReplaceOne(model => model.Id == id, data);
            return data;
        }
        

    }
}