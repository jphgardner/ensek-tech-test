﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace Ensek.Api.Core.Interfaces
{
    public abstract class IModel
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
    }
}