﻿namespace Ensek.Api.Core.Validator
{
    public interface IValidator<T> {
        bool Validate(T t);
    }
}