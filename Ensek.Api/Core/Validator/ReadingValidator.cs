﻿using Ensek.Api.Core.Collections;
using Ensek.Api.Core.Models;

namespace Ensek.Api.Core.Validator
{
    public class ReadingValidator: IValidator<Reading>
    {
        private AccountCollection _accounts;

        public ReadingValidator(AccountCollection accounts)
        {
            _accounts = accounts;
        }

        public bool Validate(Reading reading)
        {
            return _accounts.AccountIdExists(reading.AccountId) &&
                   reading.Value > 0 && 
                   reading.Value.ToString().Length == 5;
        }
    }
}