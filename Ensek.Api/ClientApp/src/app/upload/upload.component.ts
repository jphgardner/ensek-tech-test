import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {UploadService} from "../services/upload.service";
import {catchError, map} from "rxjs/operators";
import {HttpErrorResponse, HttpEventType} from "@angular/common/http";
import {of} from "rxjs";
import {MatSnackBar} from "@angular/material/snack-bar";

@Component({
  selector: 'app-upload',
  templateUrl: './upload.component.html',
  styleUrls: ['./upload.component.css']
})
export class UploadComponent implements OnInit {
  @ViewChild("fileUpload", {static: false}) public fileUpload: ElementRef;
  public file = null;

  constructor(private uploader: UploadService, private snackbar: MatSnackBar) { }

  ngOnInit() {
  }

  onClick() {
    const fileUpload = this.fileUpload.nativeElement;
    fileUpload.onchange = () => {
      for (let index = 0; index < fileUpload.files.length; index++)
      {
        const file = fileUpload.files[index];
        this.file = { data: file, inProgress: false, progress: 0 };
      }
      this.upload();
    };
    fileUpload.click();
  }

  upload() {
    const formData = new FormData();
    formData.append('file', this.file.data);
    this.file.inProgress = true;
    this.uploader.upload(formData).pipe(
      map(event => {
        switch (event.type) {
          case HttpEventType.UploadProgress:
            this.file.progress = Math.round(event.loaded * 100 / event.total);
            break;
          case HttpEventType.Response:
            return event;
        }
      }),
      catchError((error: HttpErrorResponse) => {
        this.file.inProgress = false;
        return of(`${this.file.data.name} upload failed.`);
      })
    ).subscribe((event: any) => {
      if (typeof (event) === 'object') {
        console.log(event.body);
        this.file.success = event.body.Done;
        this.file.failed = event.body.Failed;
      }
    });
  }
}
