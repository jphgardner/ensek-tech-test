﻿import {IModel} from "./IModel";

export class Account extends IModel {
  public AccountId: number;
  public FirstName: string;
  public LastName: string;
}
