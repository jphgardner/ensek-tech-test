import {Inject, Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class UploadService {

  constructor (private _http: HttpClient,
               @Inject('BASE_URL') private _baseUrl) {
    console.log("baseUrl", this._baseUrl);
  }

  public upload(formData): Observable<any> {
    return this._http.post<any>(`${this._baseUrl}api/upload`, formData, {
      reportProgress: true,
      observe: 'events'
    });
  }

}
