import {Inject, Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import { Account } from "../models/account";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class AccountsService {

  constructor(private _http: HttpClient, @Inject('BASE_URL') private _baseUrl: string) { }

  public get(): Observable<Account[]> {
    return new Observable<Account[]>(obs => {
      this._http.get(`${this._baseUrl}api/account`).subscribe((res: any) => {
        console.log("response", res);
        if(res.Success) {
          obs.next(res.Results);
        }
      });
    });
  }

  public getById(id: string): Observable<Account> {
    return new Observable<Account>(obs => {
      this._http.get(`${this._baseUrl}api/account/${id}`).subscribe((res: any) => {
        console.log("response", res);
        if(res.Success) {
          obs.next(res.Result);
        }
      });
    });
  }

}
