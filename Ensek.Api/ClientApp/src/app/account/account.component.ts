import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {Observable, Subject} from "rxjs";
import {takeUntil} from "rxjs/operators";
import {AccountsService} from "../services/accounts.service";
import { Account } from "../models/account";

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.css']
})
export class AccountComponent implements OnInit, OnDestroy {

  private _unsub: Subject<any>;
  public id: string = null;
  public accounts: Observable<Account[]>;
  public account: Observable<Account>;

  constructor(private _router: Router,
              private _route: ActivatedRoute,
              private _accountService: AccountsService) {
    this._unsub = new Subject();
  }

  ngOnInit() {
    this._route.paramMap.pipe(
      takeUntil(this._unsub)
    ).subscribe((params) => {
      if(params.has('id')) {
        this.id = params.get('id');

        //Load the Account
        this.account = this._accountService.getById(this.id).pipe(takeUntil(this._unsub));
      }
      else {
        this.id = null;

        //Load all accounts
        this.accounts = this._accountService.get().pipe(takeUntil(this._unsub));
      }
    });
  }

  open(id: string) {
    this._router.navigate(['account', id]);
  }

  ngOnDestroy(): void {
    this._unsub.next();
    this._unsub.complete();
  }
}
