﻿using Ensek.Api.Core.Collections;
using Microsoft.AspNetCore.Mvc;

namespace Ensek.Api.Controllers
{
    [ApiController]
    [Route("api/Account")]
    public class AccountController
    {
        private AccountCollection _accounts;

        public AccountController(AccountCollection accountCollection)
        {
            _accounts = accountCollection;
        }

        [HttpGet]
        public IActionResult Get()
        {
            var accounts = _accounts.Get();
            if (accounts.Count > 0)
            {
                return new OkObjectResult(new
                {
                    Success = true,
                    Results = accounts
                });
            }

            return new NotFoundObjectResult(new
            {
                Code = "account-empty",
                Message = "Accounts is Empty"
            });
        }
        
        [HttpGet("{id}")]
        public IActionResult GetById(string id)
        {
            var account = _accounts.GetById(id);
            if (account != null)
            {
                return new OkObjectResult(new
                {
                    Success = true,
                    Result = account
                });
            }

            return new NotFoundObjectResult(new
            {
                Code = "account-not-exists",
                Message = "Accounts does not exist"
            });
        }
    }
}