﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ensek.Api.Core.Collections;
using Ensek.Api.Core.CSVMappers;
using Ensek.Api.Core.Models;
using Ensek.Api.Core.Validator;
using Microsoft.AspNetCore.Mvc;
using MongoDB.Driver;
using Newtonsoft.Json;
using TinyCsvParser;

namespace Ensek.Api.Controllers
{
    [ApiController]
    [Route("api/Upload")]
    public class UploadController: ControllerBase
    {
        private AccountCollection _accounts;
        private ReadingCollection _readings;

        public UploadController(AccountCollection accountCollection, ReadingCollection readingCollection)
        {
            _accounts = accountCollection;
            _readings = readingCollection;
        }

        [HttpPost, DisableRequestSizeLimit]
        public IActionResult GetById()
        {
            if (Request.Form.Files.Count == 1)
            {
                var file = Request.Form.Files[0];
                //Read the csv contents
                
                CsvParserOptions csvParserOptions = new CsvParserOptions(true, ',');
                ReadingMapping csvMapper = new ReadingMapping();
                CsvParser<Reading> csvParser = new CsvParser<Reading>(csvParserOptions, csvMapper);

                var results = csvParser.ReadFromStream(file.OpenReadStream(), Encoding.ASCII)
                    .ToList();

                var validator = new ReadingValidator(_accounts);

                var errors = results.Where(r => !r.IsValid).ToList();
                var done = results.Where(r => r.IsValid && validator.Validate(r.Result)).ToList();
                var failedValidation = results.Where(r => r.IsValid && !validator.Validate(r.Result)).ToList();

                var failedCount = errors.Count + failedValidation.Count;
                
                var operations = new List<WriteModel<Reading>>();
                if (done.Count > 0)
                {
                    //Create a Database Write 
                    foreach (var r in done)
                    {
                        if (!_readings.Exists(r.Result))
                        {
                            operations.Add(new InsertOneModel<Reading>(r.Result));
                        }
                        else
                        {
                            failedCount++;
                        }
                    }
                }
                
                if (operations.Count > 0)
                {
                    _readings.GetCollection().BulkWrite(operations);
                }
                
                return new OkObjectResult(new
                {
                    Success = true,
                    Done = operations.Count,
                    Failed = failedCount
                });
            }
            

            return new NotFoundObjectResult(new
            {
                Code = "account-not-exists",
                Message = "Accounts does not exist"
            });
        }
    }
}